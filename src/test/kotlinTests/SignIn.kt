package kotlinTests

import actors.ExternalSupplyAdministrator
import org.testng.annotations.Test
import screens.LoginScreen
import tasks.Login
import tasks.Navigate

class SignIn {
    var externalSupplyAdministrator = ExternalSupplyAdministrator()

    @Test
    fun successfulSignIn() {
        Navigate.to(LoginScreen.url())
        Login.`as`(externalSupplyAdministrator)
    }

}
