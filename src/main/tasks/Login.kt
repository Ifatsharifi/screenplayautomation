package tasks

import actions.Click
import actions.Enter
import actors.ExternalSupplyAdministrator
import screens.LoginScreen

class Login : Task() {
    companion object {

        fun `as`(actor: ExternalSupplyAdministrator) {
            Enter.textInto(Task.driver, actor.username, LoginScreen.usernameBox)
            Enter.textInto(Task.driver, actor.password, LoginScreen.passwordBox)
            Click.on(Task.driver, LoginScreen.loginButton)
        }
    }

}
