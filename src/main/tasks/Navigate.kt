package tasks

class Navigate : Task() {
    companion object {

        fun to(url: String) {
            Task.driver.get(url)
        }
    }


}
