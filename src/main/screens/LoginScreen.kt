package screens

import org.openqa.selenium.By

class LoginScreen : Screen() {
    companion object {
        internal var url = "https://mui-qa.fyber.com/login"

        var usernameBox = By.id("userName-input")
        var passwordBox = By.cssSelector("#password-input > div > input")
        var loginButton = By.id("signInSubmitButton")

        fun url(): String {
            return url
        }
    }

}