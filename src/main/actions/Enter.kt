package actions

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver

object Enter {
    fun textInto(driver: WebDriver, text: String, selector: By) {
        driver.findElement(selector).sendKeys(text)
    }
}